package com.srmconnect.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srmconnect.repository.entity.Quotation;
import com.srmconnect.repository.enums.RepositoryNames;

/**
 * Repository pour les devis
 */
@Repository(RepositoryNames.QUOTATION_REPOSITORY)
public interface QuotationRepository extends CrudRepository<Quotation, Long> {
}
