package com.srmconnect.common.enums;

/**
 * Enum des types de contact
 */
public enum ContactType {

	PROSPECT,
	CLIENT;
}
