package com.srmconnect.services.enums;

/**
 * Classe définissant la liste des services
 */
public class ServiceNames {
    
    public static final String QUOTATION_SERVICE = "quotationService";
    
    /**
     * Constructeur
     */
    private ServiceNames() {
    }
}
