package com.srmconnect.services.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.srmconnect.model.dto.ProductDTO;
import com.srmconnect.repository.entity.Product;

/**
 * Mapper pour les produit 
 */
@Mapper
public interface ProductMapper {

	ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
	
	Product productDTOToProduct(ProductDTO productDTO);
	
	ProductDTO productToProductDTO(Product product);

}
