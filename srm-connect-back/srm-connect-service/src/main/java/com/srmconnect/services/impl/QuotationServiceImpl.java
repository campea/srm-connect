package com.srmconnect.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srmconnect.model.dto.QuotationDTO;
import com.srmconnect.repository.entity.Quotation;
import com.srmconnect.repository.interfaces.QuotationRepository;
import com.srmconnect.services.enums.ServiceNames;
import com.srmconnect.services.interfaces.QuotationService;
import com.srmconnect.services.mapper.QuotationMapper;

/**
 * Service des devis
 */
@Service(ServiceNames.QUOTATION_SERVICE)
public class QuotationServiceImpl implements QuotationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuotationServiceImpl.class);

    @Autowired
    private QuotationRepository quotationRepository;
    
    @Autowired
    private QuotationMapper quotationMapper;
    
    /**
     * {@inheritDoc}
     */
	@Override
	public QuotationDTO getQuotation(Long id) {
		LOGGER.debug("Récupération du devis : " + id);
		return quotationMapper.quotationToQuotationDTO(quotationRepository.findOne(id));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuotationDTO saveQuotation(QuotationDTO quotation) {
		LOGGER.debug("Enregistrement d'un nouveau devis");
		Quotation savedQuotation = quotationRepository.save(quotationMapper.quotationDTOToQuotation(quotation));
		return quotationMapper.quotationToQuotationDTO(savedQuotation);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<QuotationDTO> getQuotations() {
		LOGGER.debug("Récupération des devis");
		return quotationMapper.quotationsToQuotationDTOs(quotationRepository.findAll());
	}
}
