package com.srmconnect.services.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.srmconnect.model.dto.ContactDTO;
import com.srmconnect.repository.entity.Contact;

/**
 * Mapper pour les contacts
 */
@Mapper
public interface ContactMapper {

	ContactMapper INSTANCE = Mappers.getMapper(ContactMapper.class);

	Contact contactDTOToContact(ContactDTO contactDTO);

	ContactDTO contactToContactDTO(Contact contact);

	List<Contact> contactDTOsToContacts(Iterable<ContactDTO> contacts);

	List<ContactDTO> contactsToContactDTOs(Iterable<Contact> contacts);

}
