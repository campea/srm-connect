package com.srmconnect.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.messaging.handler.annotation.Payload;

import com.srmconnect.model.dto.QuotationDTO;
import com.srmconnect.rest.enums.KafkaContants;
import com.srmconnect.rest.enums.Packages;
import com.srmconnect.services.interfaces.QuotationService;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { Packages.MODEL, Packages.SERVICES, Packages.REST, Packages.REPOSITORIES,
		Packages.COMMON, Packages.DATABUS })
@EntityScan({ Packages.REPOSITORIES })
@EnableJpaRepositories(Packages.REPOSITORIES)
@EnableKafka
public class SrmConnectApplication {

	@Autowired
	private QuotationService quotationService;
	
	public static void main(String[] args) {
		SpringApplication.run(SrmConnectApplication.class, args);
	}

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;

	@Bean
	public Map<String, Object> consumerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, KafkaContants.EARLIEST);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "quotationId");
		return props;
	}

	@Bean
	public ConsumerFactory<String, QuotationDTO> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), null, new JsonDeserializer(QuotationDTO.class));
	}

	@Bean
	public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, QuotationDTO>> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, QuotationDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		return factory;
	}

	@KafkaListener(topics = "quotation")
	public void receive(@Payload QuotationDTO quotation) {
		System.out.println(quotation);
		quotationService.saveQuotation(quotation);
	}
}
