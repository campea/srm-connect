package com.srmconnect.rest.enums;

/**
 * Constante pour kafka
 */
public final class KafkaContants {
    
    public static final String EARLIEST = "earliest";

}
