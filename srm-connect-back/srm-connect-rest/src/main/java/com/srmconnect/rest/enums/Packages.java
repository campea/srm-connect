package com.srmconnect.rest.enums;

/**
 * Classe définissant le chemin des packages
 */
public class Packages {

    /**
     * Constructeur
     */
    private Packages() {
    }
    
    public static final String MODEL = "com.srmconnect.model";
    
    public static final String REPOSITORIES = "com.srmconnect.repository";

    public static final String REST = "com.srmconnect.rest";

    public static final String SERVICES = "com.srmconnect.services";
    
    public static final String COMMON = "com.srmconnect.common";
    
    public static final String DATABUS = "com.srmconnect.databus";
}
