import { Product } from './product';
import { Contact } from './contact';

export class Quotation {
  id: number;
  products: Array<Product> = new Array<Product>();
  priceHT: number;
  priceTTC: number;
  tax: number;
  discount: number;
  contact: Contact = new Contact();
}
