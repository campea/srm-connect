import { Quotation } from './quotation';
import { Company } from './company';

export class Contact {
  id: number;
  firstName: string;
  lastName: string;
  type: string;
  company: Company = new Company();
  quotations: Array<Quotation> = new Array<Quotation>();
}
