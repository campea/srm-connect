import { Contact } from './contact';
import { Company } from './company';
export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    phone: string;
    products: Array<Contact> = new Array<Contact>();
  }