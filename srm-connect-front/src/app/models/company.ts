export class Company {
  id: number;
  name: string;
  type: string;
  naf: string;
  siren: string;
  siret: string;
  streetNumber: number;
  street: string;
  zipCode: string;
  city: string;
  country: string;
  phone: string;
}
