import { EditQuotationComponent } from './components/quotation/edit-quotation/edit-quotation.component';
import { Routes } from '@angular/router';
import { QuotationListComponent } from './components/quotation/quotation-list/quotation-list.component';

export const appRoutes: Routes = [
  {
    path: 'quotation/quotation-list',
    component: QuotationListComponent
  },
  {
    path: 'quotation/edit/:id',
    component: EditQuotationComponent
  }
];
