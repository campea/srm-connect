import { MaterialModule } from './shared/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { RouterModule } from '@angular/router';
import { QuotationService } from './services/quotation/quotation.service';

import { AppComponent } from './app.component';
import { QuotationListComponent } from './components/quotation/quotation-list/quotation-list.component';
import { appRoutes } from './routerConfig';
import { NavComponent } from './components/shared/nav/nav.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditQuotationComponent } from './components/quotation/edit-quotation/edit-quotation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'angular-calendar';
import { NgbTimepickerModule, NgbDatepickerModule, NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    QuotationListComponent,
    NavComponent,
    EditQuotationComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
    CalendarModule.forRoot(),
    NgbDatepickerModule.forRoot(),
    NgbTimepickerModule.forRoot(),
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule
  ],
  providers: [QuotationService],
  bootstrap: [AppComponent]
})
export class AppModule { }